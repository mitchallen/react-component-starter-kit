react-component-starter-kit 
==
react component starter kit files used by a react-component-starter script.
--

## Usage

This kit is not meant to be used directly. Instead, it is cloned by a script that will use it to initialize __React Component__ projects. 
   
* * *

## Kit Files

A starter kit should copy these files to the root or subfolder of a new project and use sed to replace values.

### Template Files

* ```kit/*```

* * *
 
## Repo(s)

* [bitbucket.org/mitchallen/node-starter-kit.git](https://bitbucket.org/mitchallen/node-starter-kit.git)

* * *

## Contributing

In lieu of a formal style guide, take care to maintain the existing coding style.
Add unit tests for any new or changed functionality. Lint and test your code.

* * *

## Version History

#### Version 0.1.0 

* initial release

* * *
