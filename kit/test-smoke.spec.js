import React from 'react';
import CLASS_NAME from '../src/index';

describe('CLASS_NAME', () => {
  const wrapper = shallow(<CLASS_NAME />);

  it('root element should be a div', () => {
    expect(wrapper.type()).to.eql('div');
  });
});
