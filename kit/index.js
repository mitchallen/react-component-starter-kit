/*
    Module: FULL_PKG_NAME
    Author: AUTHOR_NAME
*/

import React from 'react';
// import PropTypes from 'prop-types';

class CLASS_NAME extends React.Component {
  render() {
    return (
    	<div>
      		<div>Package: FULL_PKG_NAME</div>
      		<div>Component: CLASS_NAME</div>
      	</div>
    );
  }
}

// CLASS_NAME.propTypes = {
//   // someProp: PropTypes.isRequired,
// };

export default CLASS_NAME;
